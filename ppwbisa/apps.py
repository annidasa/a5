from django.apps import AppConfig


class PpwbisaConfig(AppConfig):
    name = 'ppwbisa'
